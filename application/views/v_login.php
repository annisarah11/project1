</!DOCTYPE html>
<html>
<head>
  <title>
    Login
  </title>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets')?>/css/css_login.css">
<body>
    <div class="container">
  
  <div class="row" id="pwd-container">
    <div class="col-md-4"></div>
    
    <div class="col-md-4">
      <section class="login-form">
        <form method="post" action="<?php echo site_url('login/auth')?>" role="login">
          <?php
          $info = $this->session->flashdata('info');
          if (!empty($info)){
            echo $info;
          }
          ?>
          <h1 class ="form-signin-heading">Selamat Datang</h1>
         <h3 class ="form-signin-heading">Sistem Informasi Publikasi Ilmiah Dosen</h3>
         <hr class="colorgraph">
          <input type="email" name="email" class="form-control" placeholder="Email" required autofocus/>
          <input type="password" class="form-control input-lg" name="password" placeholder="Password" required="" />
          <div class="pwstrength_viewport_progress"></div>     
          <button type="submit" name="go" class="btn btn-lg btn-primary btn-block">Sign in</button>
          <div>
            <a href="<?php echo site_url('register')?>">Daftar</a> or <a href="#">reset password</a>
          </div>
          
        </form>
        <div class="form-links">
          <a href="<?php echo site_url('home')?>">Masuk Sebagai Guest</a>
        </div>
      </section>  
      </div>      
      <div class="col-md-4"></div>
  </div>
</div>
</body>
</html>