<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url()."assets"?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('username');?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">

      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">CARI ARTIKEL</li>
                <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Judul/Penulis/Afiliasi...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
        <li class="header">MENU UTAMA</li>
        <li>
          <a href="<?php echo site_url('page/admin')?>">
            <i class="fa fa-home"></i> <span>Beranda</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li>
          <a href="<?php echo site_url('about')?>">
            <i class="fa fa-info-circle"></i> <span>Tentang</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Pengguna</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url('admin/tbl_admin')?>"><i class="fa fa-circle-o"></i> Administrator</a></li>
            <li><a href="<?php echo site_url('admin/tbl_penguji')?>"><i class="fa fa-circle-o"></i> Penguji</a></li>
            <li><a href="<?php echo site_url('admin/tbl_author')?>"><i class="fa fa-circle-o"></i> Penulis</a></li>
          </ul>
        </li>
            <li>
          <a href="<?php echo site_url('admin/tbl_scrape')?>"><i class="fa fa-refresh"></i> <span>Scraping Data</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">Admin</small>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i>
            <span>Publikasi Ilmiah</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo site_url('admin/tbl_publikasi')?>"><i class="fa fa-circle-o"></i> Semua</a></li>
            <li><a href="<?php echo site_url('admin/tbl_jurnal')?>"><i class="fa fa-circle-o"></i> Jurnal</a></li>
            <li><a href="<?php echo site_url('admin/tbl_konferensi')?>"><i class="fa fa-circle-o"></i> Konferensi</a></li>
            <li><a href="<?php echo site_url('admin/tbl_buku')?>"><i class="fa fa-circle-o"></i> Buku</a></li>
          </ul>
        </li>
        <li>
          <a href="<?php echo site_url('admin/tbl_afiliasi')?>">
            <i class="fa fa-bank"></i> <span>Afiliasi</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>