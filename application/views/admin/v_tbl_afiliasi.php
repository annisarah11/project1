
<!DOCTYPE html>
<html>
<?php $this->load->view('admin/01_head')?>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <?php $this->load->view('admin/02_header')?>
  <?php $this->load->view('admin/03_sidebar')?>


  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="card">
              <div class="card-header d-flex p-0">
                <h3 class="card-title p-3">Afiliasi</h3>
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link active" href="data_pelamar" >Semua</a></li>
                  <li class="nav-item"><a class="nav-link" href="data_alternatif" >Fakultas</a></li>
                  <li class="nav-item"><a class="nav-link" href="data_nilai" >Departemen</a></li>
                  <li class="nav-item"><a class="nav-link" href="data_kriteria" >Lembaga</a></li>
                </ul>
              </div>
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th><div style="text-align: center;">ID</div></th>
                  <th><div style="text-align: center;">Nama Jurnal</div></th>
                  <th><div style="text-align: center;">Penerbit</div></th>
                  <th><div style="text-align: center;">URL</div></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>1</td>
                  <td>TELKOMNIKA (Telecommunication Computing Electronics and Control)</td>
                  <td>Universitas Ahmad Dahlan</td>
                  <td>www.uad.ac.id</td>
                </tr>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

      </section>
  </div>
<?php $this->load->view('admin/04_footer')?>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
