
<!DOCTYPE html>
<html>
<?php $this->load->view('admin/01_head')?>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <?php $this->load->view('admin/02_header')?>
  <?php $this->load->view('admin/03_sidebar')?>


  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Jurnal
        </h1>
    </section>
    <section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th><div style="text-align: center;">ID</div></th>
                  <th><div style="text-align: center;">Judul</div></th>
                  <th><div style="text-align: center;">Halaman</div></th>
                  <th><div style="text-align: center;">Penulis</div></th>
                  <th><div style="text-align: center;">Penerbit</div></th>
                  <th><div style="text-align: center;">Tempat Terbit</div></th>
                  <th><div style="text-align: center;">Tahun Terbit</div></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>1</td>
                  <td>Kiat Sukses Meraih Hibah Penelitian Pengembangan</td>
                  <td>20+XIV</td>
                  <td>Tatik Sutarti, Edi Irawan</td>
                  <td>Deepublish</td>
                  <td>Yogyakarta</td>
                  <td>2017</td>
                </tr>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

      </section>
  </div>
<?php $this->load->view('admin/04_footer')?>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
