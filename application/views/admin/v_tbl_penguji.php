
<!DOCTYPE html>
<html>
<?php $this->load->view('admin/01_head')?>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <?php $this->load->view('admin/02_header')?>
  <?php $this->load->view('admin/03_sidebar')?>


  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Penguji
        <small>Daftar Penguji</small>
      </h1>
      <ol class="breadcrumb">
                  <td>
                    <a type="button" href="<?php echo site_url('add_penguji')?>" class="btn btn-block btn-success btn-sm"><i class="fa fa-fw fa-plus"></i> Tambah Penguji</a>
                  </td>
      </ol>
    </section>
    <section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>User ID</th>
                  <th>Email</th>
                  <th>Username</th>
                  <th>Password</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>Trident</td>
                  <td>Internet
                    Explorer 4.0
                  </td>
                  <td>Win 95+</td>
                  <td> 4</td>
                  <td style="min-width:100px;">
                    <a class="btn btn-warning btn-sm" href="#" ><i class="fa fa-fw fa-pencil-square-o"></i> Ubah</a>
                    <a class="btn btn-danger btn-sm" href="#" onclick="return confirm('Yakin ingin menghapus?')"><i class="fa fa-fw fa-trash-o"></i> Hapus</a>
                  </td>
                </tr>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

      </section>
  </div>
  <!-- /.content-wrapper -->


</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<?php $this->load->view('admin/04_footer')?>
</body>
</html>
