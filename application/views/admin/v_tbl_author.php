
<!DOCTYPE html>
<html>
<?php $this->load->view('admin/01_head')?>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <?php $this->load->view('admin/02_header')?>
  <?php $this->load->view('admin/03_sidebar')?>


  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Penulis
        <small>Data Author</small>
      </h1>
      <ol class="breadcrumb">
                  <td>
                    <a type="button" href="<?php echo site_url('#')?>" class="btn btn-block btn-success btn-sm"><i class="fa fa-fw fa-plus"></i> Tambah Author</a>
                  </td>
      </ol>
    </section>
    <section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                <div class="box-tools">

                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">

                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table id="example1" class="table table-bordered table-striped">
                <tr>
                  <th>ID</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Afiliasi</th>
                  <th>Url Google Scholar</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
                <tr>
                  <td>183</td>
                  <td>John Doe</td>
                  <td>11-7-2014</td>
                  <td>11-7-2014</td>
                  <td>11-7-2014</td>
                  <td><span class="label label-success">Sudah Terverifikasi</span></td>
                  <td style="min-width:100px;">
                    <a class="btn btn-success btn-sm" href="#" ><i class="fa fa-fw fa-check-circle-o"></i> Verifikasi</a>
                    <a class="btn btn-warning btn-sm" href="#" ><i class="fa fa-fw fa-pencil-square-o"></i> Ubah</a>
                    <a class="btn btn-danger btn-sm" href="#" onclick="return confirm('Yakin ingin menghapus?')"><i class="fa fa-fw fa-trash-o"></i> Hapus</a>
                  </td>
                </tr>
                <tr>
                  <td>183</td>
                  <td>John Doe</td>
                  <td>11-7-2014</td>
                  <td>11-7-2014</td>
                  <td>11-7-2014</td>
                  <td><span class="label label-danger">Tidak Terverifikasi</span></td>
                  <td style="min-width:100px;">
                    <a class="btn btn-success btn-sm" href="#" ><i class="fa fa-fw fa-check-circle-o"></i> Verifikasi</a>
                    <a class="btn btn-warning btn-sm" href="#" ><i class="fa fa-fw fa-pencil-square-o"></i> Ubah</a>
                    <a class="btn btn-danger btn-sm" href="#" onclick="return confirm('Yakin ingin menghapus?')"><i class="fa fa-fw fa-trash-o"></i> Hapus</a>
                  </td>
                </tr>
                <tr>
                  <td>183</td>
                  <td>John Doe</td>
                  <td>11-7-2014</td>
                  <td>11-7-2014</td>
                  <td>11-7-2014</td>
                  <td><span class="label label-warning">Belum Terverifikasi</span></td>
                  <td style="min-width:100px;">
                    <a class="btn btn-success btn-sm" href="#" ><i class="fa fa-fw fa-check-circle-o"></i> Verifikasi</a>
                    <a class="btn btn-warning btn-sm" href="#" ><i class="fa fa-fw fa-pencil-square-o"></i> Ubah</a>
                    <a class="btn btn-danger btn-sm" href="#" onclick="return confirm('Yakin ingin menghapus?')"><i class="fa fa-fw fa-trash-o"></i> Hapus</a>
                  </td>
                </tr>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
<?php $this->load->view('admin/04_footer')?>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
