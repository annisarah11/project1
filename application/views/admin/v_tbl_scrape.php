
<!DOCTYPE html>
<html>
<?php $this->load->view('admin/01_head')?>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <?php $this->load->view('admin/02_header')?>
  <?php $this->load->view('admin/03_sidebar')?>


  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Scraping
        <small>Data artikel yang terlah diekstraksi</small>
      </h1>
      <ol class="breadcrumb">

                  <td>
                    <a type="button" href="<?php echo site_url('#')?>" class="btn btn-block btn-success btn-sm"><i class="fa fa-fw fa-refresh"></i> Perbarui Data</a>
                  </td>
      </ol>
    </section>
    <section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="box">

            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th><div style="text-align: center;">ID</div></th>
                  <th><div style="text-align: center;">Judul</div></th>
                  <th><div style="text-align: center;">Penulis</div></th>
                  <th><div style="text-align: center;">Publikasi</div></th>
                  <th><div style="text-align: center;">Penerbit</div></th>
                  <th><div style="text-align: center;">Deskripsi</div></th>
                  <th><div style="text-align: center;">Cited by</div></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>1</td>
                  <td style="width:150px">Perancangan Jaringan Sensor Terdistribusi untuk Pengaturan Suhu, Kelembaban dan Intensitas Cahaya
                  </td>
                  <td style="width: 130px">Bimo Ananto Pamungkas, Adian Fatchur Rochim, Eko Didik Widianto</td>
                  <td>Jurnal</td>
                  <td>Jurnal Teknologi dan Sistem Komputer</td>
                  <td><font size="-1">Makalah ini membahas tentang perancangan sistem sensor terdistribusi untuk memonitor suhu, kelembaban dan intensitas cahaya di rumah kaca ( greenhouse) menggunakan board Arduino Uno. Sistem terdiri atas 2 node sensor-aktuator dan 1 node kontroler yang terhubung ke jaringan ethernet menggunakan board Ethernet Shield. Node sensor-aktuator dengan sensor DHT 11 berfungsi mengambil informasi lingkungan berupa suhu, kelembaban udara, dan intensitas cahaya, menjalankan aktuasi berupa emulasi nyala lampu LED dan berkomunikasi dengan node kontroler yang akan mengolah data dengan kebel serial sebagai perangkat komunikasi antar node. Data-data pemantauan dan antarmuka kontrol pengguna disediakan oleh node kontroler yang dapat diakses secara online lewat browser web. Kemampuan sistem untuk memonitor lingkungan dalam rumah kaca, dan akses data lingkungan lewat web membuat pemantauan dan pengelolaan tanaman dapat dilakukan secara otomatis dan terkontrol.</font></td>
                  <td>10</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td style="width:150px">Sistem Pengukur Suhu dan Kelembaban Ruang Server
                  </td>
                  <td style="width: 130px">Muhammad Fahmi Awaj, Adian Fatchur Rochim, Eko Didik Widianto</td>
                  <td>Jurnal</td>
                  <td>Jurnal Teknologi dan Sistem Komputer</td>
                  <td><font size="-1">Makalah ini membahas tentang perancangan sistem pengukur suhu dan kelembaban ruang server yang dapat meningkatkan efisiensi daya listrik dengan menggunakan arduino. Sistem terdiri dari sensor DHT 11 yang berfungsi untuk mengambil data berupa suhu dan kelembaban ruang server kemudian ditampilkan dalam</font></td>
                  <td>6</td>
                </tr>

                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>

      </section>
  </div>
<?php $this->load->view('admin/04_footer')?>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
